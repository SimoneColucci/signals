function mesh = readMesh(fileName)

% read mesh from gales .m-type file
% usage in main: "mesh = readMesh(mesh_name)"

% Chiara Montagna, INGV Pisa, 5/2/2021
% chiara.montagna@ingv.it

% ======== INPUT =================================================
% mesh file name mesh.m
% ================================================================

% ======== OUTPUT ================================================
% mesh is a structure containing:
% n_nodi: double, number of nodes
% n_elementi: double, number of elements
% pos_x_nod: n_nodi array, x position of nodes (sorted)
% pos_y_nod: n_nodi array, y position of nodes (sorted)
% nod_of_element: n_elementi quadruples, nodes of elements (sorted)
% ================================================================

file = fopen(fileName);
posx = [];
posy = [];
nodi_of_ele = [];
a = fscanf(file,'%s',1);
aa = 1 - strcmp(a,'Element');
while (aa)
    if (strcmp(a,'Node'))
        nnodi = fscanf(file,'%i',1);
        posx(nnodi) = fscanf(file,'%e',1);
        posy(nnodi) = fscanf(file,'%f',1);
    end
    a = fscanf(file,'%s',1);
    aa = 1 - strcmp(a,'Element');
end
aa = 1 - strcmp(a,'');
while (aa)
  if (strcmp(a,'Element'))
    a = fscanf(file,'%s',1);  
    nelementi = fscanf(file,'%i',1);
    intero = fscanf(file,'%i',1);
    intero = fscanf(file,'%i',1);
    for i = 1:4
      nodi_of_ele(nelementi,i)= fscanf(file,'%e',1);
    end
  end    
  a = fscanf(file,'%s',1);
  aa = 1 - strcmp(a,'');
end
mesh.n_nodi = nnodi;
mesh.n_elementi = nelementi;
mesh.pos_x_nod = posx;
mesh.pos_y_nod = posy;
mesh.nod_of_element = nodi_of_ele;
disp('end of readMesh')
return
