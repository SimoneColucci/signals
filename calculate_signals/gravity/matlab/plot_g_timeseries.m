% load gravity anomaly data and plot figures of gravity variations
% in time, one for each station

% Chiara Montagna, Simone Colucci; INGV Pisa, 5/2/2021
% chiara.montagna@ingv.it simone.colucci@ingv.it


% ============ INPUT ===========================================
% gravity data file g*.mat - gravity anomaly results from integrate.m, contains variables:
%                          t time
%                          x horizontal receivers coordinate
%                          y vertical receivers coordinate
%                          twoDgx horizontal gravity anomaly for the 2D simulated system
%                          twoDgy vertical gravity anomaly for the 2D simulated system
%                          gx horizontal gravity anomaly for a 3D extended system
%                          gy vertical gravity anomaly for a 3D extended system
% ==============================================================

% =========== OUTPUT ==========================================
% figures gravity_change_x_IX.jpg - gravity anomaly time series at station IX
% ==============================================================

clear all
close all

% =========== TO BE SET ========================================
% save figures
save_path = '/home/simone/Work/Data/repository_gales/Etna/synthetic_geophysical_signals/gravimetry/figures/time_series/';

% load data
load '/home/simone/Work/Data/repository_gales/Etna/synthetic_geophysical_signals/gravimetry/data/g'
% ==============================================================

mkdir (save_path)

t = t/3600; 
for ix = 1:length(x)
  fig1 = figure(1);
  clf;
  set(gcf,'Visible','off','Renderer','ZBuffer');
  plot(t,-1.e8*(gy(:,ix)-gy(1,ix)),'LineWidth',2)
  title(['Gravity change at x = ' num2str(x(ix)) ' m'])
  xlabel('Time (hours)');
  ylabel('\Delta g (\mu Gal)');
  print(fig1, '-djpeg',[save_path 'gravity_change_x_' num2str(x(ix)) ...
                      '.jpg']); 
end
