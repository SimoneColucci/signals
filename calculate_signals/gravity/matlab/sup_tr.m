function S = sup_tr(x1,y1,x2,y2,x3,y3)

% calculate triangle area
% input triangle verteces are [x1,y1],[x2,y2] and [x3,y3]
% output area is S

P = cross([x2-x1,y2-y1,0],[x3-x1,y3-y1,0]);
S = 0.5*(abs(P(3)));
return