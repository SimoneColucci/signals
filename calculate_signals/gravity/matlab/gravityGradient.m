% calculate gravity gradient at synthetic stations

% Chiara Montagna, INGV Pisa, 07/2020
% chiara.montagna@ingv.it

% ================ INPUT ==============================
% results from deformation.m, specifically:
%         g_SIM_z-Nm.mat - gravity anomaly for simulation SIM at vertical coordinate -N m, contains variables:
%                          t time
%                          x horizontal receivers coordinate
%                          y vertical receivers coordinate
%                          twoDgx horizontal gravity anomaly for the 2D simulated system
%                          twoDgy vertical gravity anomaly for the 2D simulated system
%                          gx horizontal gravity anomaly for a 3D extended system
%                          gy vertical gravity anomaly for a 3D extended system
%         g_SIM_zNm.mat - gravity anomaly for simulation SIM at vertical coordinate N m, contains the same variables as above

% =====================================================

% ================ OUTPUT =============================
% figure containing vertical gravity gradient in space
% =====================================================

% =============== TO BE SET ===========================
% data files
dataFileBelow = '../data/g_e13_z-10m.mat';
dataFileAbove = '../data/g_e13_z10m.mat';
step = 20;     % vertical distance between heights above [m]
% =====================================================


load(dataFileBelow);
gyMinus10 = gy;

load(dataFileAbove);
gradSpace = (gy - gyMinus10)/step;

plot(x,gradSpace)
xlabel('x, m')
ylabel('Gravity acceleration gradient, gal/m')