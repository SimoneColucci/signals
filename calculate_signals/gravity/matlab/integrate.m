% produce and save time-series of gravity acceleration due to density
% variations at depth calculated by p-gales-1.0.2

% !! needs sup_tr.m, readMesh.m and openFile.m !!

% Chiara P. Montagna, Simone Colucci; INGV Pisa, 4/1/2021
% chiara.montagna@ingv.it simone.colucci@ingv.it

% ========== OUTPUT ========================================
% file saveFile - synthetic gravity anomaly at selected stations in time, contains variables:
%                          t time
%                          x horizontal receivers coordinate
%                          y vertical receivers coordinate
%                          twoDgx horizontal gravity anomaly for the 2D simulated system
%                          twoDgy vertical gravity anomaly for the 2D simulated system
%                          gx horizontal gravity anomaly for a 3D extended system
%                          gy vertical gravity anomaly for a 3D extended system
% ==========================================================

% =========== TO BE SET ====================================
tempo_iniziale = 6592;       % initial time [s]
numero_tempi = 6592;         % number of time samples
deltat = 1;                  % time step [s]
simulation = 'e13';

% path to the 'results' and 'simulation' folders
path = ['/Volumes/TOURO/etna/' simulation '/rawData/']; 
% mesh file
meshFile = [path 'simulation/e13.m']; 

% save data
savePath = ['./data/'];
saveFile = ['g_' simulation '_z.mat'];   

% 0 calculates and saves mesh and cells' weights, 1 loads them
calcolo_pesi = 1;               

% 3rd dimension
L = 1; 

% synthetic gravimeters' coordinates (horizontal and vertical)
% x and y must have the same length
x = [-10000 -6000 -5000 -4000 -2500 -1500 -1000 -900 -750 -600 -500 ...
      -250 -100 -50 0 50 100 250 500 600 750 900 1000 1500 2500 5000 4000 ...
      6000 10000]; 
y  = [-10 -10 -10 -10 -10 -10 -10 -10 -10 -10 -10 -10 -10 -10 -10 -10 -10 ...
      -10 -10 -10 -10 -10 -10 -10      -10      -10      -10   -10 -10]; 
% =========================================================

mkdir(savePath);
numero_letture = floor((numero_tempi-tempo_iniziale)/deltat)+1;

G = 6.6726e-11;  % gravitational constant;

if(calcolo_pesi == 0)
    mesh = readMesh(meshFile);
    disp('after readMesh')
    save ([savePath simulation 'Mesh.mat'],'mesh');
    
    peso = zeros(1,mesh.n_nodi);
    
    for k = 1:mesh.n_elementi
        a1 = sup_tr(...
            mesh.pos_x_nod(mesh.nod_of_element(k,1)),...
            mesh.pos_y_nod(mesh.nod_of_element(k,1)),...
            mesh.pos_x_nod(mesh.nod_of_element(k,2)),...
            mesh.pos_y_nod(mesh.nod_of_element(k,2)),...
            mesh.pos_x_nod(mesh.nod_of_element(k,4)),...
            mesh.pos_y_nod(mesh.nod_of_element(k,4)));
        a2 = sup_tr(...
            mesh.pos_x_nod(mesh.nod_of_element(k,2)),...
            mesh.pos_y_nod(mesh.nod_of_element(k,2)),...
            mesh.pos_x_nod(mesh.nod_of_element(k,3)),...
            mesh.pos_y_nod(mesh.nod_of_element(k,3)),...
            mesh.pos_x_nod(mesh.nod_of_element(k,1)),...
            mesh.pos_y_nod(mesh.nod_of_element(k,1)));
        a3 = sup_tr(...
            mesh.pos_x_nod(mesh.nod_of_element(k,3)),...
            mesh.pos_y_nod(mesh.nod_of_element(k,3)),...
            mesh.pos_x_nod(mesh.nod_of_element(k,4)),...
            mesh.pos_y_nod(mesh.nod_of_element(k,4)),...
            mesh.pos_x_nod(mesh.nod_of_element(k,2)),...
            mesh.pos_y_nod(mesh.nod_of_element(k,2)));
        a4 = sup_tr(...
            mesh.pos_x_nod(mesh.nod_of_element(k,4)),...
            mesh.pos_y_nod(mesh.nod_of_element(k,4)),...
            mesh.pos_x_nod(mesh.nod_of_element(k,1)),...
            mesh.pos_y_nod(mesh.nod_of_element(k,1)),...
            mesh.pos_x_nod(mesh.nod_of_element(k,2)),...
            mesh.pos_y_nod(mesh.nod_of_element(k,2)));
        peso(mesh.nod_of_element(k,1)) = ...
            peso(mesh.nod_of_element(k,1))+(a1+a2+a4)/6; 
        peso(mesh.nod_of_element(k,2)) = ...
            peso(mesh.nod_of_element(k,2))+(a1+a2+a3)/6; 
        peso(mesh.nod_of_element(k,3)) = ...
            peso(mesh.nod_of_element(k,3))+(a2+a3+a4)/6; 
        peso(mesh.nod_of_element(k,4)) = ...
            peso(mesh.nod_of_element(k,4))+(a1+a3+a4)/6; 
    end
    save([savePath 'weights_' simulation '.mat'],'peso');
else
    load([savePath 'weights_' simulation '.mat']);
    load([savePath simulation 'Mesh.mat']);
end

fx = zeros(mesh.n_nodi,length(x));
fy = zeros(mesh.n_nodi,length(x));

for j = 1:length(x)
    d2 = (x(j)-mesh.pos_x_nod(1:mesh.n_nodi)).^2+(y(j)- ...
                                                  mesh ...
                                                  .pos_y_nod(1: ...
                                                      mesh.n_nodi)).^2;   
    d3 = sqrt(d2+0.25*L^2);
    dd = d2.*d3;
    fx(:,j) = (peso.*(mesh.pos_x_nod - x(j))./dd)';
    fy(:,j) = (peso.*(mesh.pos_y_nod - y(j))./dd)';
end

gx = zeros(numero_letture,length(x));
gy = zeros(numero_letture,length(x)); 

tt = 1;
for t = tempo_iniziale:deltat:numero_tempi
    if t<10
        nomefile = [path ['results/bin_densities/bin_densities_0-9/' ...
                          'density'] num2str(t)];   
    else
        nomefile = [path 'results/bin_densities/bin_densities_' ...
                    num2str(floor(t/10)) '0-' num2str(floor(t/10)) ...
                    '9/density' num2str(t)];   
    end
    density = openFile(nomefile);
    ddensity = repmat(density,1,length(x));
    twoDgx(tt,:) = G*sum(fx.*ddensity);
    gx(tt,:) = twoDgx(tt,:)*L;
    twoDgy(tt,:) = G*sum(fy.*ddensity);
    gy(tt,:) = twoDgy(tt,:)*L;
    tt = tt + 1;
end
t = (tempo_iniziale:deltat:numero_tempi);
save([savePath saveFile],'x','y','t','gx','gy','twoDgx', ...
     'twoDgy');