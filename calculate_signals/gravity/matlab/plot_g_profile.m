% load gravity anomaly data and plot gravity variation in space,
% one line for each time selected

% Chiara Montagna, INGV Pisa, 6/11/2020
% chiara.montagna@ingv.it

% ============ INPUT ===========================================
% gravity data file g*.mat - gravity anomaly results from integrate.m, contains variables:
%                          t time
%                          x horizontal receivers coordinate
%                          y vertical receivers coordinate
%                          twoDgx horizontal gravity anomaly for the 2D simulated system
%                          twoDgy vertical gravity anomaly for the 2D simulated system
%                          gx horizontal gravity anomaly for a 3D extended system
%                          gy vertical gravity anomaly for a 3D extended system
% ==============================================================

% =========== OUTPUT ==========================================
% gravity_change_profile.jpg - gravity anomaly in space at selected times
% ==============================================================

close all;

% ============ TO BE SET =======================================
% load gravity data
load '/home/simone/Work/Data/repository_gales/Etna/synthetic_geophysical_signals/gravimetry/data/g';

% set times to plot 
% t is time variable loaded from gravity data
timesIndeces = length(t);

% save figures
save_path =  '/home/simone/Work/Data/repository_gales/Etna/synthetic_geophysical_signals/gravimetry/figures/profile/';

% ==============================================================

mkdir(save_path)

t = t/3600; % tempo in ore
for i = timesIndeces
  fig1 = figure(1);
  set(gcf,'Renderer','ZBuffer');
  plot(x, - 1.e8 * (gy(i,:) - gy(1,:)),'k','LineWidth',2)
  xlabel('x (m)','FontSize',20);
  ylabel('\Delta g (\mu Gal)','FontSize',20);
  set(gca,'FontSize',18);
  hold on
  print(fig1, '-djpeg',[save_path 'gravity_change_profile.jpg']); 
end