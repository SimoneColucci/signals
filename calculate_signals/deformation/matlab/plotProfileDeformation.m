% plot deformation profile, actual and normalized values


% Chiara P. Montagna, Simone Colucci; INGV Pisa, 4/1/2021
% chiara.montagna@ingv.it

% ================== INPUT ==================================

% results from deformation.m, specifically:
%              displacement.mat - displacement results, contains variables: 
%                                 XR receivers coordinates
%                                 XS source coordinates
%                                 dto time step
%                                 t time                   
%                                 Utotx horizontal displacement in time t at stations XR
%                                 Utotz vertical displacement in time t at stations XR
%              normalized_displacement.mat - normalized displacement results, contains variables: 
%                                 XR receivers coordinates
%                                 XS source coordinates
%                                 dto time step
%                                 t time                   
%                                 norm_Utotx normalized horizontal displacement in time t at stations XR
%                                 norm_Utotz vertical displacement in time t at stations XR
%              strain.mat - strain results, contains:
%                           XR receivers coordinates 
%                           XS source coordinates    
%                           dto time step            
%                           t time                   
%                           exx horizontal strain in time t at stations XR
%                           ezz vertical strain in time t at stations XR
%                           theta volumentric strain in time t at stations XR
% ============================================================

% ========================= OUTPUT =====================================

% figures:
%         vertical_displacement_profile.jpg - vertical displacement in space, at selected times tt
%         normalized_vertical_displacement_profile.jpg - normalized vertical displacement in space, at selected times tt
%         horizontal_displacement_profile.jpg - horizontal displacement in space, at selected times tt
%         normalized_horizontal_displacement_profile.jpg - normalized horizontal displacement in space, at selected times tt
%         volumetric_strain_profile.jpg - volumetric strain in space, at selected times tt

% ============================================================

close all

% ============================ TO BE SET =================================

% data files
load( '/home/simone/Work/Data/repository_gales/Etna/synthetic_geophysical_signals/deformation/data/mat/displacement.mat');
load( '/home/simone/Work/Data/repository_gales/Etna/synthetic_geophysical_signals/deformation/data/mat/strain.mat');
load( '/home/simone/Work/Data/repository_gales/Etna/synthetic_geophysical_signals/deformation/data/mat/normalized_displacement.mat');

% path for saving figures
path_figs = '/home/simone/Work/Data/repository_gales/Etna/synthetic_geophysical_signals/deformation/figures/';

% time samples to be plotted in figures
tt = 'last'; % a vector of times

% watch figures being created  
visible_flag = 'on';

% ========================================================================

mkdir(path_figs)

if (strcmp('last',tt)) 
    tt = length(Utotx); 
end

% create figures

xr_km = XR(:,1)/1000;

%% displacement

f1=figure(1);
set(gcf,'Visible',visible_flag)
set(gcf,'Position',[1 1 1601 571])
for i = tt
    plot(xr_km,Utotz(i,:),'k','Linewidth',2)
    hold on
end 
if (length(tt)>1)
title(['Vertical displacement in the range ' num2str(tt(1)) ':' num2str(tt(2) - tt(1)) ':' num2str(tt(end)) 's'],'Fontsize',16)
else
 title(['Vertical displacement at ' num2str(tt(1)) 's'],'Fontsize',16)
 end
set(gca,'FontSize',20)
xlabel('Receivers (km)','FontSize',20)
ylabel('Vertical displacement (m)','FontSize',20)

f2 = figure(2);
set(gcf,'Visible',visible_flag)
set(gcf,'Position',[1 1 1601 571])
for i = tt
    plot(xr_km,norm_Utotz(tt,:),'k','Linewidth',2)
    hold on
end
if (length(tt)>1)
title(['Normalized vertical displacement in the range ' num2str(tt(1)) ':' num2str(tt(2) - tt(1)) ':' num2str(tt(end)) 's'],'Fontsize',16)
else
 title(['Normalized vertical displacement at ' num2str(tt(1)) 's'],'Fontsize',16)
 end
set(gca,'FontSize',20)
%set(gca,'YLim',[0 1.01])
xlabel('Receivers (km)','FontSize',20)
ylabel('Normalized vertical displacement','FontSize',20)

f3 = figure(3);
set(gcf,'Visible',visible_flag)
set(gcf,'Position',[1 1 1601 571])
for i = tt
    plot(xr_km,Utotx(i,:),'k','Linewidth',2)
    hold on
end
if (length(tt)>1)
title(['Horizontal displacement in the range ' num2str(tt(1)) ':' num2str(tt(2) - tt(1)) ':' num2str(tt(end)) 's'],'Fontsize',16)
else
 title(['Horizontal displacement at ' num2str(tt(1)) 's'],'Fontsize',16)
 end
set(gca,'FontSize',20)
xlabel('Receivers (km)','FontSize',20)
ylabel('Horizontal displacement (m)','FontSize',20)

f4 = figure(4);
set(gcf,'Visible',visible_flag)
set(gcf,'Position',[1 1 1601 571])
for i = tt
    plot(xr_km,norm_Utotx(i,:),'k','Linewidth',2)
    hold on
end
if (length(tt)>1)
title(['Normalized horizontal displacement in the range ' num2str(tt(1)) ':' num2str(tt(2) - tt(1)) ':' num2str(tt(end)) 's'],'Fontsize',16)
else
 title(['Normalized horizontal displacement at ' num2str(tt(1)) 's'],'Fontsize',16)
 end
set(gca,'FontSize',20)
xlabel('Receivers (km)','FontSize',20)
ylabel('Horizontal displacement (m)','FontSize',20)

% save figures 
print(f1,'-djpeg',[path_figs 'vertical_displacement_profile.jpg'])
print(f2,'-djpeg',[path_figs 'normalized_vertical_displacement_profile.jpg'])
print(f3,'-djpeg',[path_figs 'horizontal_displacement_profile.jpg'])
print(f4,'-djpeg',[path_figs 'normalized_horizontal_displacement_profile.jpg'])

%% volumetric strain

f5=figure(5);
set(gcf,'Visible',visible_flag)
set(gcf,'Position',[1 1 1601 571])
for i = tt
    plot(xr_km,theta(i,:),'k','Linewidth',2)
    hold on
end 
if (length(tt)>1)
title(['Volumetric strain in the range ' num2str(tt(1)) ':' num2str(tt(2) - tt(1)) ':' num2str(tt(end)) 's'],'Fontsize',16)
else
 title(['Volumetric strain at ' num2str(tt(1)) 's'],'Fontsize',16)
 end
set(gca,'FontSize',20)
xlabel('Receivers (km)','FontSize',20)
ylabel('Vertical displacement (m)','FontSize',20)

% save figures 
print(f5,'-djpeg',[path_figs 'volumetric_strain_profile.jpg'])

