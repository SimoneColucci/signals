% preprocessing for the calculation of the static deformation

% load force sources time series sourceNO.mat and create a file forceSources.txt with 5 columns: number and coordinates of said sources, and forces x and y at at the last simulated time

% input files contain time series of forces at specific points in 2D space
% the files MUST be called sourceNUMBER.mat (e.g. source1.mat); 
% they contain 4 variables:
% x, double - horizontal coordinate of source
% y, double - vertical coordinate of source
% Fx, array of double - time series of horizontal force at source
% Fy, array of double - time series of vertical force at source

% Chiara Montagna, INGV Pisa, November 2020
% chiara.montagna@ingv.it

% ============ TO BE SET ===================================================

% path for sourceNO.mat files
sourcesPath = '../data/sources/*/*.mat';

% ==========================================================================

sourcesList = dir(sourcesPath);

% load source data
for i = 1:length(sourcesList)
    load([sourcesList(i).folder '/' sourcesList(i).name]);
    xCoords(i) = x;
    yCoords(i) = y;
    sourceNo(i) = sscanf(sourcesList(i).name,'source%u');
    forceX(i) = Fx(end);
    forceY(i) = Fy(end);
end

% sort nodes and all variables
[sourcesNo,sortedIndeces] = sort(sourceNo);
xCoordsSorted = xCoords(sortedIndeces);
yCoordsSorted = yCoords(sortedIndeces);
forceXSorted = forceX(sortedIndeces);
forceYSorted = forceY(sortedIndeces);

% create table to save data
data = table(sourcesNo,xCoordsSorted,yCoordsSorted,forceXSorted,forceYSorted);
writetable(data,'forceSources.txt','Delimiter','\t');