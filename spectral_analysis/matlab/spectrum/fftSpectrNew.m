function  [FreqSeries_db,fax] = fftSpectrNew(matrix,varargin)
 
    %% FFTSPECTRNEW (v3.0): Compute several fft spectrum of a time series input matrix.
        %  Wrapper utility function of FFT algorithm  in MATLAB.
        %  Input matrix should be sized Nx2. Column one time axis, column two values.
        %
        %  USAGE: [FreqSeries,FreqAxis] = fftSpectr(matrix,varargin)
        %
        %  Where varargin is specified with the pair (... "Name",value ...):
        %         "matrix" - matrix's path name or just matrix name of TS in input.
        %         "Dt" - If not specified, is calculated from the input TimeAxis
        %         "Type" - char variable, indicating one of the listed spectrum
        %                  to be done: 'fft' (returns the FFT results--> NO PLOT),
        %                              'fft-abs','fft-real','fft-phase',
        %                              'ESD' (Energy Spectral Density),
        %                              'PSD' (Power Spectral Density).
        %         "ZeroPad" - integer variable, indicating the power of 2 to
        %                      be applied as Zero Padding of the signal in input.
        %                      (example: 3 --> 2^3=8 --> an 8 length zero vector
        %                      is added to the signal). If =0, nextpow2(lengthsignal)
        %                      window is used in fft, if=[] a lengthsignal window
        %                      is used instead.
        %         "Taper" - if TRUE, matrix is windowed before FFT.[TRUE]
        %         "TaperType" - 'cosine' ---> Tuckey window
        %                       'hann' ---> Hannin gwindow
        %                       'BH4'---> 4-term Blackman-Harris window
        %                       'KB' ---> Kaiser-Bessel window
        %         "TaperValue"- (cosine) REAL: for percentage aperture [0-1]
        %                       (hann) CHAR: equivalent to 'sflag' in hann funct.
        %                                    {'periodic' or 'symmetric'}
        %                       (BH4) CHAR: equivalent to 'sflag' in blackmanharris funct.
        %                                   {'periodic' or 'symmetric'}
        %                       (KB) REAL: "beta" value of the kaiser function. [0-1]
        %                                   beta affects the sidelobe attenuation of
        %                                   the Fourier transform of the window.
        %  --------------------------------------------------------------- 
        %         "Plot" - if TRUE, execute the plot-block, additional parameters
        %                  are listed below. If not specified, the following value
        %                  will be useless.
        %         "LinSpec" - line spec. for plot
        %         "Yaxis" - 'dB'/['lin'] mean which scale to plot the Y value
        %                   The "decibel" option is valid for every type
        %                   EXCEPT 'fft','fft-phase'.
        %         "Xaxis" - 'log'/['lin'] mean which scale to plot the X value
        %         "SubPlot" - [vector] vector for subplot, default [1,1,1]
        %         "FigHandle" - handle figure to plot
        %
        %  NOTICE: Is important to window the signal
        %          in order to avoid noise in the spectrum
        %
        %  AUTHOR: Matteo Bagagli, 11/2015 @ INGV.PI
        % chiara.montagna@ingv.it simone.colucci@ingv.it
%% ========================================================================
%   Add to path
% =========================================================================        
addpath(genpath('./utils'));

%% ========================================================================
%   Checks
% =========================================================================
if nargin < 1
    error(['fftSpectrNew: ERROR insterting values. ' ...
        'USAGE: [FreqSeries,fax]=' ...
        'fftSpectrNew(type,zeroPadd,matrix,varargin)']);
else
    disp('fftSpectrNew: Starting spectral analysis ...');
end

%% ========================================================================
%   Defaults & Variable
% =========================================================================
Def=struct('Type','fft-abs', ...
    'ZeroPad',0, ...
    'Dt',[], ...
    'Taper',1, ...
    'TaperType','cosine', ...
    'TaperValue',0.05, ...
    'Plot',0 , ...
    'SubPlot',[1,1,1], ...
    'LinSpec','-b', ...
    'FigHandle',[], ...
    'Yaxis','lin', ...
    'Xaxis','lin');
Args=parseArgs(Def,varargin);
%
if(ischar(matrix))
    signal=load(matrix);
else
    signal=matrix;
end

%% ========================================================================
%   Tapering / Windowing
% =========================================================================
if Args.Taper
    signal=taperTS(signal,Args.TaperType,Args.TaperValue);
end

%% ========================================================================
%   Variables
% =========================================================================
if ~isempty(Args.Dt)
    dt=Args.Dt;
else
    dt=matrix(2,1)-matrix(1,1);
end

Fs=1/dt;
sigData=signal(:,2);
%sigData=signal(:,2)-mean(signal(:,2));
lenSig=length(sigData);

%% ========================================================================
%   Zero Padding
% =========================================================================
if Args.ZeroPad~=0
    nfft=2^nextpow2(lenSig*Args.ZeroPad);
elseif isempty(Args.ZeroPad)
    nfft=lenSig;
else
    nfft=2^nextpow2(lenSig);
end

%% ========================================================================
%   Creating FFTxlim([0 0.1]);
% =========================================================================
FFT=fft(sigData,nfft);
HalfPosPoints=ceil((nfft+1)/2);  % Taking only the Half (positive) part of the FFT
FFT=FFT(1:HalfPosPoints);
fax=(0:(HalfPosPoints-1))*Fs/(nfft-1);
fax=fax';

%% ========================================================================
%   Switch Type
% =========================================================================
switch Args.Type
    case 'fft'                  % fft
        FreqSeries=cat(2,fax,FFT);
    case 'fft-abs'              % Magnitude
        MAG=abs(FFT);
        MAG_db=20*log10(MAG);
        FreqSeries=cat(2,fax,MAG);
        FreqSeries_db=cat(2,fax,MAG_db);
    case 'fft-real'             % Real spectrum
        REAL=real(FFT);
        REAL_db=20*log10(REAL);
        FreqSeries=cat(2,fax,REAL);
        FreqSeries_db=cat(2,fax,REAL_db);
    case 'fft-phase'            % Phase spectrum
        PHASE=angle(FFT);
        FreqSeries=cat(2,fax,PHASE);
    case 'ESD'                  % Energy Spectral Density
        ESD=(abs(FFT)).^2;
        ESD_db=10*log10(ESD);
        FreqSeries=cat(2,fax,ESD);
        FreqSeries_db=cat(2,fax,ESD_db);
    case 'PSD'                  % Power Spectral Density
        PSD=(abs(FFT).^2)/(Fs*nfft);
        PSD(2:end-1) = 2*PSD(2:end-1); % *see notes (1)
        PSD_db=10*log10(PSD);
        FreqSeries=cat(2,fax,PSD);
        FreqSeries_db=cat(2,fax,PSD_db);
    otherwise
        error(['fftSpectrNew: ERROR insterting values. ' ...
            'Specify the ouput desired: see ''type''(digit help fftSpectrNew) !!! ']);
end

%% ========================================================================
%   Plot
% =========================================================================
if Args.Plot && ~strcmp(Args.Type,'fft')
    disp('fftSpectr: Plotting ...')
    ToPlot=FreqSeries;
    if strcmp(Args.Yaxis,'dB') && ~any(strcmp(Args.Type,'fft-phase'))
        ToPlot=FreqSeries_db;
    else
        disp(['fftSpectrNew: No decibel (dB) scale for the specified ''Type''', ...
            'using the linear scale for the Y axis']);
    end
    % Search figure
    if isempty(Args.FigHandle)
        figure
    else
        figure(Args.FigHandle)
    end
    % Search subplot
    subplot(Args.SubPlot(1),Args.SubPlot(2),Args.SubPlot(3))
    % plot
    plot(ToPlot(:,1),ToPlot(:,2),Args.LinSpec);
    % Xaxis
    if strcmp(Args.Xaxis,'log'); set(gca,'Xscale','log'); end
elseif Args.Plot && strcmp(Args.Type,'fft')
    disp(['fftSpectrNew: No-Plot is available at the moment for the ', ...
        '''fft'' type ...choose another type instead !!!']);
else
    disp('fftSpectrNew: No-Plot selected ...')
end

% #########################################################################
% ############################   Notes   ##################################
% #########################################################################
% (1) Because the signal is real-valued, you only need power estimates for
% the positive or negative frequencies. In order to conserve the total power,
% multiply all frequencies that occur in both sets -- the positive and negative
% frequencies -- by a factor of 2. Zero frequency (DC) and the Nyquist freq.
% do not occur twice.

% (2) The deciBel scale is calculeted with a factor of 10 for POWERS & ENERGIES
% and 20 for the AMPLITUDE signals.
% #########################################################################

%% ========================================================== Nested Funct.
    function [timeSeries,window]=taperTS(file,type,tap0pt,varargin)
        %% TAPERTS
        %  Function for "windowing" a Time Series with different type of window.
        %
        %  USAGE: [timeSeries]=taperTS(file,type,tapOpt,varargin)
        %
        %  INPUT:   file --> timeSeries to analyze
        %           type --> 'cosine' ---> Tuckey window
        %                    'hann' ---> Hannin gwindow
        %                    'BH4'---> 4-term Blackman-Harris window
        %                    'KB' ---> Kaiser-Bessel window
        %           tapOpt --> (cosine) REAL: for percentage aperture [0-1]
        %                      (hann) CHAR: equivalent to 'sflag' in hann funct.
        %                                  {'periodic' or 'symmetric'}
        %                      (BH4) CHAR: equivalent to 'sflag' in blackmanharris funct.
        %                                  {'periodic' or 'symmetric'}
        %                      (KB) REAL: "beta" value of the kaise function. [0-1]
        %                                  beta affects the sidelobe attenuation of
        %                                  the Fourier transform of the window.
        %           [linSp] - line spec. for plot
        %           [vectSubplt] - vector for subplot
        %           [figHandle] - handle figure to plot
        %  OUTPUT: Time Series processed
        %  Author: Matteo Bagagli 02/2015
        
        %% SetUp
        if nargin < 3
            error('taperTS: ERROR insterting values. USAGE: [timeSeries]=taperTS(file,type,tap0pt,varargin)')
        else
            text=['taperTS: Starts using a ',type,' windowing !!!'];
            disp(text);
        end
        if(ischar(file))
            DATA=load(file);
        else
            DATA=file;
        end
        winLen=length(DATA); % window's length
        
        %% Switch
        if strcmp(type,'cosine') && isreal(tap0pt)      % Tukey (cosine) window
            window=tukeywin(winLen,tap0pt);
            DATA_WIN=DATA(:,2).*window;
            timeSeries=cat(2,DATA(:,1),DATA_WIN);
        elseif strcmp(type,'hann') && ischar(tap0pt)    % Hanning window
            window=hann(winLen,tap0pt);
            DATA_WIN=DATA(:,2).*window;
            timeSeries=cat(2,DATA(:,1),DATA_WIN);
        elseif strcmp(type,'BH4') && ischar(tap0pt)     % 4-term Blackman-Harris window
            window=blackmanharris(winLen,tap0pt);
            DATA_WIN=DATA(:,2).*window;
            timeSeries=cat(2,DATA(:,1),DATA_WIN);
        elseif strcmp(type,'KB') && isreal(tap0pt)      % Kaiser-Bessel window
            window=kaiser(winLen,tap0pt);
            DATA_WIN=DATA(:,2).*window;
            timeSeries=cat(2,DATA(:,1),DATA_WIN);
        else
            error('taperTS: Error with TYPE & TAPER-OPTIONS !!! Aborting...');
        end
        
        %% Plot
        
        if ~isempty(varargin)
            disp('taperTS: Plotting ...')
            if length(varargin) == 1
                plot(timeSeries(:,1),timeSeries(:,2),varargin{1}); % ,'MarkerSize',8
            elseif length(varargin) == 2
                disp('taperTS: No figure handle specified ... new figure created !!!');
                figure;
                vectSubplt=varargin{2};
                if isempty(vectSubplt) ~= 1
                    subplot(vectSubplt(1),vectSubplt(2),vectSubplt(3));
                end
                plot(timeSeries(:,1),timeSeries(:,2),varargin{1}); %,'MarkerSize',8
            elseif length(varargin) == 3
                figure(varargin{3});
                vectSubplt=varargin{2};
                if isempty(vectSubplt) ~= 1
                    subplot(vectSubplt(1),vectSubplt(2),vectSubplt(3));
                end
                plot(timeSeries(:,1),timeSeries(:,2),varargin{1}); %,'MarkerSize',8
            end
        else
            disp('taperTS: No plot selected !!')
        end
    end
%
end % end MAIN