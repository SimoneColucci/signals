function [S,F,T] = compute_spectrogram(sig,nseg,foverlap,fs,nfig)
    
% Chiara Montagna, Simone Colucci; INGV Pisa, 8/2/2021
% chiara.montagna@ingv.it simone.colucci@ingv.it

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% [S,F,T] = function compute_spectrogram(sig,nseg,foverlap,fs,ifig)
%
% INPUT
% sig = signal to be analyzed, array
% nseg = number of segments in which the signal is divided
% foverlap = fraction of overlapping of segments
% fs = sampling frequency
% nfig = figure number; 0 or negative integers mean no figure
%
% OUTPUT
% S, F, T are the outputs of the spectrogram function:
% S is the short-time Fourier transform of the input signal, sig. Each column of S contains an estimate of the short-term, time-localized frequency content of sig.
% F is a vector of normalized frequencies
% T is a vector of time instants at which the spectrogram is computed
% spectrogram is plotted if nfig > 0
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

l = round(length(sig)/nseg);
noverlap = round(l*foverlap);
nfft = 2^(nextpow2(l));

[S,F,T] = spectrogram(sig,l,noverlap,nfft,fs);

if(nfig > 0)
  figure(nfig)
  pcolor(T/3600,F,log10(abs(S)));
  colorbar;
  shading flat
  xlabel('time')
  ylabel('log_{10}(frequency)')
end