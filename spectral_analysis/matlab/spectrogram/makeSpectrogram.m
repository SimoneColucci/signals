function makeSpectrogram(matrix,varargin)

%% spectrogram: Compute and/or plot spectrogram of a time series input matrix.
%  Wrapper utility function of pspectrum  in MATLAB.
%  Input matrix should be sized Nx2. First column time axis, second one values.
%
%  USAGE: [P,F,T] = makeSpectrogram(matrix,varargin)
%  coose output at line 75
%
%  Where varargin is specified with the pair (... "Name",value ...):
%         "matrix" - matrix's path name or just matrix name of TS in input.
%         "TimeLimits" - ([t,T]); default ([min(matrix(:,1)),max(matrix(:,1))])
%         "FrequencyLimits" - [n,N]; default ([min(matrix(:,2)),max(matrix(:,2))])
%         "overlapPercent" - any value between 1 and 100; default 50
%  ---------------------------------------------------------------
%         "Plot" - if TRUE, execute the plot-block, additional parameters
%                  are listed below. If not specified, the following value
%                  will be useless.
%         "LinSpec" - line spec. for plot
%         "Yaxis" - 'log'/['lin'] mean whic scale to plot the X value
%         "Xaxis" - 'log'/['lin'] mean whic scale to plot the X value
%         "SubPlot" - [vector] vector for subplot, default [1,1,1]
%         "FigHandle" - handle figure to plot
%
%  AUTHOR: Simone Colucci, 06/2020, INGV, sezione di Pisa
%  simone.colucci@ingv.it
%% ========================================================================
%   Add to path
% =========================================================================
addpath(genpath('./utils'));

%% ========================================================================
%   Checks
% =========================================================================
if nargin < 1
    error(['spectrogram: ERROR insterting values. ' ...
        'USAGE: [P,F,T]=' ...
        'spectrogram(matrix,varargin)']);
else
    disp('calculating spectrogram ...');
end

%% ========================================================================
%   Defaults & Variable
% =========================================================================
if(ischar(matrix))
    signal=load(matrix);
else
    signal=matrix;
end

tL = seconds([min(matrix(:,1)) max(matrix(:,1))]); % seconds
fL = [0 1./(2.*min(diff(matrix(:,1))))]; % Hz

Def=struct('timeLimits',tL, ...
    'frequencyLimits',fL, ...
    'overlapPercent',50, ...
    'Plot',0 , ...
    'SubPlot',[1,1,1], ...
    'LinSpec','-b', ...
    'FigHandle',[], ...
    'Yaxis','lin', ...
    'Xaxis','lin');
Args=parseArgs(Def,varargin);

%% Compute spectrogram

% Index into signal time region of interest
TS_ROI = squeeze(matrix(:,2));
timeValues = matrix(:,1);
TS_ROI = timetable(seconds(timeValues(:)),TS_ROI,'VariableNames',{'Data'});
TS_ROI = TS_ROI(timerange(Args.timeLimits(1),Args.timeLimits(2),'closed'),1);

% Compute spectral estimate
% Run the function call below without output arguments to plot the results
%[P,F,T] = 
pspectrum(TS_ROI, ...
    'spectrogram', ...
    'FrequencyLimits',Args.frequencyLimits, ...
    'OverlapPercent',Args.overlapPercent);


end
