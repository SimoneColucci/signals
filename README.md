# Signals

A suite of MATLAB scripts to produce and analyze synthetic geophysical signals.

[calculate_signals](calculate_signals/) contains MATLAB scripts to calculate deformation and gravity anomaly at the Earth surface due to time-varying, non-homogeneous deep sources.

[deformation](calculate_signals/deformation/matlab/):
+ [deformation.m](calculate_signals/deformation/matlab/deformation.m)
  is the driver to calculate time series of deformation due to
  selected sources at depth
+ [Green2D.m](calculate_signals/deformation/matlab/Green2D.m)
  calculates the analytic convolution of the Green's Function with an
  external force, in a 2D approximation
+ [Somigliana.m](calculate_signals/deformation/matlab/Somigliana.m) calculates the static approximation to the Green's functions in an homogeneous, infinite medium
+ [plotProfileDeformation.m](calculate_signals/deformation/matlab/plotProfileDeformation.m) plots ground deformation in space at selected times
+ [plotTimseriesDeformation.m](calculate_signals/deformation/matlab/plotTimseriesDeformation.m) plots time
  series of deformation at selected synthetic receivers
+ [sourceTest.mat](calculate_signals/deformation/matlab/sourceTest.mat)
  is an exemplary source file for testing [deformation.m](calculate_signals/deformation/matlab/deformation.m)

[gravity](gravity/matlab/):
+ [integrate.m](integrate.m) calculates time series of gravity anomaly at synthetic stations integrating density variations at depth
+ [gravityGradient.m](gravityGradient.m) calculates gravity gradient at the Earth surface using results from integrate.m
+ [plot_g_profile.m](plot_g_profile.m) plots gravity anomaly in space at selected times
+ [plot_g_timeseries.m](plot_g_timeseries.m) plots time series of gravity anomaly at selected receivers

[spectral_analysis](spectral_analysis/matlab/) contains MATLAB scripts to perform spectral analysis on time series.

[spectrogram](spectrogram/):
+ [compute_spectrogram.m](compute_spectrogram.m) calculate spectrogram for a given time series
+ [makeSpectrogram.m](makeSpectrogram.m) plot spectrogram for a given time series

[spectrum](spectrum/):
+ [fftSpectrNew.m](fftSpectrNew.m) calculates and plots spectrum of a given time series

## Contributing

Pull requests are welcome. For major changes, please open an issue
first to discuss what you would like to modify.
Collaborations are also welcome, feel free to contact the developers
for volcano-related (or any other!) scientific projects.

## Developers

**Simone Colucci**: main developer; simone.colucci@ingv.it.  
**Chiara Montagna**: project PI, main developer;
chiara.montagna@ingv.it, [webpage](http://personale.pi.ingv.it/chiara-p-montagna/). 

Project developed at [INGV Pisa](http://www.pi.ingv.it/) with support
from INGV-DPC ([Italian Civil Protection
Agency](https://rischi.protezionecivile.gov.it/it/vulcanico))
collaboration framework; H2020 INFRAIA project
[EUROVOLC](https://eurovolc.eu/), grant no. 731070.

## License 
[CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/)
